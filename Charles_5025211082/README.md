# SOAL 1 
Pertama kita bisa membuat Dockerfile dengan isi command sebagai berikut : 

![Dockerfile](Charles_5025211082/dockerfile.png)

Kita mengambil image dari nginx kemudian mengekspos nya ke port 80.

Kemudian `CMD ["nginx", "-g", "daemon off;"]` akan menjalankan nginx di foreground.

Setelah itu kita dapat build image dengan nama `image-nginx:v1.0` menggunakan perintah `docker build -t image-nginx:v1.0 .`

![image-nginx](Charles_5025211082/image-nginx.png)

Setelah itu dapat kita run container-nginx dan me-foward port 80 dari container ke port 80 PC dengan command `docker run -d -p 80:80 --name container-nginx image-nginx:v1.0`

![container](Charles_5025211082/container.png)

Terakhir bisa kita cek di `https://localhost` apakah webnya berjalan.

![web](Charles_5025211082/web.png)

# SOAL 2

Untuk soal nomor 2 kita dapat mengclone dulu repo dengan command `git clone https://github.com/dptsi/nginx-proxy.git`

Setelah itu kita bisa membuat docker network di dalam direktory `nginx-proxy` dengan command `docker network create new-network`

Selanjutnya kita perlu clone sebuah repo lagi dengan command `git clone https://github.com/akunlainfiqi/hbd-clone-iyok.git`

Tetapi untuk membuat containernya masih memiliki problem dengan menggunakan dockerfile berikut :

![dockerfile_soal2](Charles_5025211082/make_container.png)

ERROR yang ditemui :

![make_container](Charles_5025211082/make_container.png)

Hanya sejauh itu saja bagian soal 2 yang dapat saya kerjakan.