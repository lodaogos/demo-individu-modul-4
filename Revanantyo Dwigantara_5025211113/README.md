1. Pada file Dockerfile kita expose port 80 agar bisa digunakan untuk mengakses HTTP
![Dockerfile](https://gitlab.com/lodaogos/demo-individu-modul-4/-/raw/main/Revanantyo%20Dwigantara_5025211113/Screenshot_from_2023-06-02_19-43-22.png)

2. Lalu, kita build dengan nama image-nginx v1.0.
![Build](https://gitlab.com/lodaogos/demo-individu-modul-4/-/raw/main/Revanantyo%20Dwigantara_5025211113/Screenshot_from_2023-06-02_19-43-50.png)

3. Setelah itu, kita run dengan nama container nginx serta memforward port 80 dari container ke port 80 PC.
![Run](https://gitlab.com/lodaogos/demo-individu-modul-4/-/raw/main/Revanantyo%20Dwigantara_5025211113/Screenshot_from_2023-06-02_19-44-02.png)

4. Setelah itu dapat diakses di localhost.
![localhost](https://gitlab.com/lodaogos/demo-individu-modul-4/-/raw/main/Revanantyo%20Dwigantara_5025211113/Screenshot_from_2023-06-02_19-44-12.png)
